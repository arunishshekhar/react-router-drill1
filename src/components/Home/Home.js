import React from "react";

function Home() {
    return (
        <div className="background-home home">
            <h1>Hi There !</h1>
            <h1>My name is Arunish Shekhar</h1>
            <h1>Designation: SDE Trainee</h1>
        </div>
    );
}

export default Home;