import React from "react";
import { Routes, Route, Link } from 'react-router-dom';
import Home from "../Home/Home";
import About from "../About/About";
import Contact from "../Contact/Contact";


function NavBar() {
    return (
        <div>
            <div className="navbar">
            <Link to='/'>Home</Link>
            <Link to="about">About</Link>
            <Link to='contact'>Contact</Link>
            </div>
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="about" element={<About />} />
                <Route path='contact' element={<Contact />} />
            </Routes>

        </div>
    );
}

export default NavBar;