import React from "react";
import "./about.css"
function About() {
    return (
        <div className="about">
            <div className="image-div">
                <img src="WhatsApp Image 2022-05-02 at 12.13.18 PM.jpeg" alt="Selfie" />
            </div>
            <div className="para-div">
                <p>I am currently a Under Grad student studying BTech in Computer Science. Along with that i am a MERN Stack Developer Trainee at MountBlue.</p>
            </div>
        </div>
    );
}

export default About;