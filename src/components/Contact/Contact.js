import React from "react";
import "./contact.css";

class ContactForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: null,
            email: null,
            number: null
        }
        this.nameChange = this.nameChange.bind(this);
        this.numberChange = this.numberChange.bind(this);
        this.emailChange = this.emailChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    handleSubmit(event) {
        let onSubmitMessage = document.getElementById("on-submit");
        if (!this.state.name || !this.state.email || !this.state.number || document.getElementById("name-msg").innerText || document.getElementById("mobile-msg").innerText || document.getElementById("email-msg").innerText) {
            onSubmitMessage.innerText = "Enter All Data"
            event.preventDefault();
        }
    }
    nameChange(event) {
        this.setState({ name: event.target.value });
        let nameMessage = document.getElementById("name-msg");
        if (/[^a-zA-Z]/.test(event.target.value)) {
            nameMessage.innerText = "Only Letter Allowed";
        }
        else {
            nameMessage.innerText = null;
        }
    }

    numberChange(event) {
        this.setState({ number: event.target.value });
        let mobileMessage = document.getElementById("mobile-msg");

        if (event.target.value.length > 10) {
            mobileMessage.innerText = "Enter a Valid Number";
        }
        else if (!/^\d+$/.test(event.target.value)) {
            mobileMessage.innerText = "Letter not Allowed";
        }
        else {
            mobileMessage.innerText = null;
        }
    }

    emailChange(event) {
        this.setState({ email: event.target.value });
        let emailMessage = document.getElementById("email-msg");
        const regx = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

        if (!regx.test(event.target.value)) {
            emailMessage.innerText = "Enter Valid Email";
        }
        else {
            emailMessage.innerText = null;
        }
    }


    render() {
        return (
            <form noValidate onSubmit={this.handleSubmit} className="form-class">
                <div>
                    {/* <p>Name :</p> */}
                    <input type="text" id="name" value={this.state.name} onChange={this.nameChange} placeholder="Name"/>
                    <p id="name-msg"></p>
                </div>
                <div>
                    {/* <p>Email :</p> */}
                    <input type="email" id="email" value={this.state.email} onChange={this.emailChange} placeholder="E-Mail ID"/>
                    <p id="email-msg"> </p>
                </div>
                <div>
                    {/* <p>Mobile No :</p> */}
                    <input type='text' id="mobile" value={this.state.number} onChange={this.numberChange} placeholder="Mobile Number"/>
                    <p id="mobile-msg"> </p>
                </div>
                <input type="submit" value="Submit" id="submit" />
                <p id="on-submit"></p>
            </form>
        )
    }
}


function Contact() {
    return (
        <div className="background-contact">
            <div className="form-div">
                <ContactForm />
            </div>
            <div>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d817.4767256551534!2d77.61160494331673!3d12.933631952754205!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae151947c69de7%3A0xf2b320fefa7ffb8c!2sMountBlue%20Technologies%20Private%20Limited!5e0!3m2!1sen!2sin!4v1651468268636!5m2!1sen!2sin" width="400" height="300" allowfullscreen="" title="Map" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
        </div>
    );
}

export default Contact;
